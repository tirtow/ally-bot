from datetime import datetime
import importlib
from os import listdir
from os.path import realpath, dirname, join
import re

from ally_exceptions import InvalidTrialTypeError
from keys import keys

# The IDs of the roles allowed to use !clear
officer_roles = frozenset(
    ["423306995018235915", "423306933085274112", "421763070642356234"])


def log(message, newline=False):
    """
    Logs the message to the console with the current time

    message -- the message to log
    """

    print("[" + str(datetime.now()) + "]: " + message)
    if newline:
        print("[" + str(datetime.now()) + "]:")


def user_is_officer(user):
    """
    Gets whether or not the given user is an officer.

    user -- the discord.Member to check

    Returns True if the user has at least one role in officer_roles, False
    otherwise
    If the bot is being ran in test environment, always returns True
    """

    if keys["env"] == "test":
        return True

    for role in user.roles:
        if role.id in officer_roles:
            return True
    return False


def to_timestamp(time):
    """
    Converts the given datetime into a timestamp

    time -- the datetime to convert

    Returns the timestamp rounded to an int
    """

    return int(round((time - datetime(1970, 1, 1)).total_seconds()))


def parse_options(message):
    """
    Parses the options passed into a dict mapping the name to the value.

    Options should be space separated in the format "name=value"

    message -- the discord.Message to parse

    Returns a dict mapping the name of each option to its value
    """

    # Pattern to match "name=value"
    pattern = "\s*[a-z]+\s*=\s*[a-z0-9]+"

    # Remove !signup init and lower case
    content = " ".join(message.content.split()[2:]).lower()

    # Find all matches
    matches = re.findall(pattern, content)

    # Map the name in each match to the value
    result = {}
    for match in matches:
        i = match.index("=")
        result[match[:i].strip()] = match[i + 1:].strip()

    return result


def get_trial_class(trial_type):
    """
    Uses reflection to load the class matching trial_type

    trial_type should be in the format <name>_trial or <name>trial for
    example: normal_trial or normaltrial.

    trial_type  -- the name of the trial class

    Returns the class

    Raises InvalidTrialTypeError if trial_type does not match the format
    above or if fails to load the module or class.
    """

    # Convert to lowercase
    trial_type = trial_type.lower()

    # Split into type and "trial"
    split = []
    if "_" in trial_type:
        # Split based on _
        split = trial_type.split("_")
    elif "trial" in trial_type:
        # Split based on index of string "trial"
        i = trial_type.index("trial")
        split = [trial_type[:i], trial_type[i:]]
    else:
        # User did not give a valid trial type
        raise InvalidTrialTypeError()

    try:
        # Try to load class
        module = importlib.import_module("trial." + "_".join(split))
        return getattr(module, "".join(map(lambda s: s.title(), split)))
    except (ModuleNotFoundError, AttributeError):
        # Class or module not found
        raise InvalidTrialTypeError()


def get_commands(client, trials):
    """
    Uses reflection to get an instance of all Commands.

    Returns a dict mapping the name of the command to the instance of the
    command.
    """

    return {name: c(client, trials) for name, c in load_module("command")}


def get_roles():
    """
    Uses reflection to get an instance of all Roles.

    Returns a dict mapping the name of the role to the instance of the role.
    """

    return {name: c() for name, c in load_module("role")}


def load_module(module_name):
    """
    Builds a generator to get the class types of all classes in the given module

    module_name     -- the name of the module

    Returns a generator of key, value pairs
    """

    path = join(dirname(realpath(__file__)), module_name)
    for f in listdir(path):
        if f.endswith("_%s.py" % module_name):
            name = f[:-3]               # Get rid of the .py
            split = name.split("_")     # Split to before and after _

            # Import the module
            module = importlib.import_module(module_name + "." +
                                             "_".join(split))

            # Yield the class type
            name = "".join(map(lambda s: s.title(), split))
            yield (name, getattr(module, name))
