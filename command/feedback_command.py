from ally_embed import AllyEmbed
from command.command import Command
from constants import COLORS
from keys import keys


class FeedbackCommand(Command):
    """
    Inherits from Command to implement the !signup feedback command.

    Allows user to provide anonymous feedback to the server.
    """

    def __init__(self, client, trials):
        """
        Gets the channel specified as "anonymous" in the keys
        """

        super().__init__(client, trials)
        self.channel = client.get_channel(keys["channels"]["anonymous"])

    async def handle(self, message):
        """
        Sends the feedback message to the channel.

        message     -- the discord.Message that was received
        """

        msg = message.content.split()[2:]
        embed = AllyEmbed(self.client.user,
                          title="Anonymous Feedback",
                          description=" ".join(msg),
                          color=COLORS["INFO"])
        await self.client.send_message(self.channel, embed=embed)

    def aliases(self):
        """
        Return a tuple of the aliases for this command.
        """

        return ("feedback", "anonymous_feedback")

    def description(self):
        """
        Returns the description of this command
        """

        return "**!signup feedback**: give anonymous feedback about " \
            "Tempering Allies"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Sends anonymous feed back to " + self.channel.mention + ".\n\n" \
            "Usage:\n" \
            "- **!signup feedback message**:"
        await self.send_help_message(channel, msg)
