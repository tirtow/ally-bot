"""
The commands that ally-bot recognizes.

Commands are loaded in at runtime using reflection. Only commands in files
ending in "_command.py" are loaded in.

Commands must follow the naming structure:
- filename:   some_name_command.py
- class name: SomeNameCommand.py

Commands **must** respond to the methods that `Command` responds to in order to
integrate properly with ally-bot. See `help(Command)` for more information.
"""
