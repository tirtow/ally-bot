from datetime import datetime, timedelta
import os
from os.path import join

from ally_embed import AllyEmbed
from ally_exceptions import InsufficentPermissionsError, \
    TrialAlreadyScheduledError
from command.command import Command
from constants import COLORS
from utils import user_is_officer, parse_options, get_trial_class


class InitCommand(Command):
    """
    Inherits from Command to implement the !signup init command.

    Provides the implementation for creating a new Trial.
    """

    async def handle(self, message):
        """
        Handles creating a new trial in the channel message was sent in.

        message -- the discord.Message that was received

        Raises InvalidTrialTypeError if an invalid trial type is given
        Raise InsufficentPermissionsError if the user is not an officer
        """

        if not user_is_officer(message.author):
            # Must be an officer to run this command
            raise InsufficentPermissionsError()

        if message.channel.id in self.trials:
            # Trial has already been scheduled in this channel
            raise TrialAlreadyScheduledError()

        # Get whether or not using legacy method
        split = message.content.lower().split()[1:]
        legacy = len(split) > 1 and split[1] in ("-l", "--legacy")

        # Get the options to create the trial with
        options = {**self._init_defaults(), **parse_options(message)}

        # Get the class
        trial_class = get_trial_class(options["type"])

        # Create the trial
        try:
            self.trials[message.channel.id] = trial_class(self.client)
            if legacy:
                self.trials[message.channel.id].init_legacy(options)
            else:
                self.trials[message.channel.id].init_message(message, options)

            # Schedule the trial
            self.trials[message.channel.id].schedule()
        except Exception:
            # Something went wrong, make sure trial is deleted
            try:
                os.remove(join("trial_info", message.channel.id + ".json"))
            except FileNotFoundError:
                pass

            # Delet from dict
            if message.channel.id in self.trials:
                del self.trials[message.channel.id]
            raise

        # Print message that trial was initialized
        msg = "Successfully scheduled trial in " + message.channel.mention
        embed = AllyEmbed(self.client.user,
                          title="Successfully Scheduled Trial",
                          description=msg,
                          color=COLORS["SUCCESS"])
        await self.client.send_message(message.channel, embed=embed)
        await self.trials[message.channel.id].when()

    def aliases(self):
        """
        Return a tuple of the aliases for InitCommand
        """

        return ("schedule", "init", "initialize", "start", "create")

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup schedule <day> [hour, minute, offset, type]:** " \
            "start signup cycle on the given day"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Initialize a trial in the channel the message was sent in.\n" \
            "A day of the week is required. The hour, offset, type, tz, and " \
            "skip parameters are optional.\n\n" \
            "**This command is restricted to __officers__.**\n\n" \
            "Usage:\n" \
            "- **!signup schedule day [hour, offset, type, tz, skip]**"
        await self.send_help_message(channel, msg)

    def _init_defaults(self):
        """
        Gets the default values for creating a trial.

        Returns the dict of default values
        """

        tomorrow = datetime.now() + timedelta(days=1)

        return {
            "year": tomorrow.year,
            "month": tomorrow.month,
            "day": tomorrow.day,
            "hour": 20,
            "minute": 0,
            "second": 0,
            "microsecond": 0,
            "offset": 25,
            "type": "NormalTrial",
            "skip": False
        }
