from command.command import Command
from tic_tac_toe import TicTacToe


class TicTacToeCommand(Command):
    """
    Inherits from Command to implement the !signup tictactoe command.

    Plays a game of Tic-Tac-Toe with the user.
    """

    def __init__(self, client, trials):
        """
        Create a dict of games mapping channel to TicTacToe
        """

        super().__init__(client, trials)
        self.games = {}

    async def handle(self, message):
        """
        Create new game if needed or take next turn if already playing a game.

        message     -- the discord.Message received
        """

        split = message.content.lower().split()[2:]

        # Check if need to start a new game
        if len(split) == 0 or message.author not in self.games:
            self.games[message.author] = TicTacToe(self.client, message.author)
            await self.games[message.author].prompt_user()

            # Add reactions to acknowledge message
            reactions = ["\U0001F1FD", "\U0001F1E6", "\U0001F1F3", "\U0001F1E9",
                         "\U0001F1F4"]
            for reaction in reactions:
                await self.client.add_reaction(message, reaction)

        # Take user's turn
        if len(split) >= 2:
            if self.games[message.author].place(split[0], split[1], "X"):
                # User won, cleanup
                await self.games[message.author].print_winner("you")
                del self.games[message.author]
            elif self.games[message.author].computer_turn():
                # Computer won, cleanup
                await self.games[message.author].print_winner("ally-bot")
                del self.games[message.author]
            else:
                # No winner, prompt user
                await self.games[message.author].prompt_user()

    def aliases(self):
        """
        Returns a tuple of the aliases for this command
        """

        return ("tictactoe", "tic-tac-toe", "ttt")

    def description(self):
        """
        Returns a description of this command
        """

        return "**!signup tictactoe:** play tic-tac-toe"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Play tic-tac-toe against ally-bot\n\n" \
            "Usage:\n" \
            "- **!signup tictactoe**:"
        await self.send_help_message(channel, msg)
