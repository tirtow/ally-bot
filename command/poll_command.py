from ally_embed import AllyEmbed
from ally_exceptions import InvalidPollError
from command.command import Command
from constants import COLORS


class PollCommand(Command):
    """
    Inherits from Command to implement the !signup poll command.

    Provides the implementation for easily creating polls using reactions as
    the method for users to vote for options.
    """

    async def handle(self, message):
        """
        Creates a poll using reactions and the options provided by the given
        message

        message -- the discord.Message with the content to create a poll
        """

        split = message.content.split()[2:]     # Remove the !signup poll

        if len(split) < 3:
            raise InvalidPollError()
        else:
            try:
                # Get the index of the options flag
                end = self._get_index_of(split, ("--options", "-o"))
                if end is None or end + 1 == len(split):
                    # No options listed after options flag
                    raise InvalidPollError
            except ValueError:
                # No options flag found
                raise InvalidPollError()

            # Send the poll message
            result = await self._send_poll_message(message, end + 1)

            # Add reactions
            await self._add_poll_reactions(result[0], result[1])

            # Pin the message
            await self.client.pin_message(message)

    def aliases(self):
        """
        Returns a tuple of the aliases for this command.
        """

        return ("poll", "vote")

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup poll <description> --options option a, option b, " \
            "...:** create a poll with the provided options."

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Start a poll using reactions.\n" \
            "A description must be provided followed by the options for the " \
            "poll.\n\n" \
            "Usage:\n" \
            "- **!signup poll description --options option 1, option 2, ...**"
        await self.send_help_message(channel, msg)

    def _get_index_of(self, it, items):
        """
        Gets the first index of any of the values in items in it.

        it      -- an iterable to search through
        items   -- the items to check for

        Returns the index of the first value in items found in it, None if none
        of the values are found.
        """

        for i, val in enumerate(it):
            if val in items:
                return i
        return None

    async def _send_poll_message(self, message, options_start):
        """
        Builds and sends the poll message to the channel message was sent in.

        message         -- the discord.Message that was received
        options_start   -- the index in split where the options list starts

        Returns the tuple
        (discord.Message that was sent, number of reactions to add)
        """

        # The alphabet
        alphabet = [chr(i) for i in range(ord('a'), ord('z') + 1)]

        # Split the message and remove !signup poll
        split = message.content.split()[2:]

        # Rejoin the split options and split on semi-colons
        options = " ".join(split[options_start:]).split(";")

        # Start the message with what the user gave as description
        msg = " ".join(split[:options_start - 1]) + "\n\n**Options:**"

        # Add the emoji and option string for each option
        index = 0
        for option in options:
            if index >= 20:
                # Discord allows max 20 reactions per message
                break

            msg += "\n:regional_indicator_" + alphabet[index] + ": " + \
                   option.strip()
            index += 1

        embed = AllyEmbed(self.client.user,
                          title="Signup Poll",
                          description=msg,
                          color=COLORS["INFO"])
        msg = await self.client.send_message(message.channel, embed=embed)

        return msg, index

    async def _add_poll_reactions(self, message, end):
        """
        Adds end reactions to the given message to be used to vote on options
        in the poll.

        message     -- the discord.Message to add reactions to
        end         -- the index to end on (exclusive)
        """

        reactions = [
            "\U0001F1E6", "\U0001F1E7", "\U0001F1E8", "\U0001F1E9",
            "\U0001F1EA", "\U0001F1EB", "\U0001F1EC", "\U0001F1ED",
            "\U0001F1EE", "\U0001F1EF", "\U0001F1F0", "\U0001F1F1",
            "\U0001F1F2", "\U0001F1F3", "\U0001F1F4", "\U0001F1F5",
            "\U0001F1F6", "\U0001F1F7", "\U0001F1F8", "\U0001F1F9",
            "\U0001F1FA", "\U0001F1FB", "\U0001F1FC", "\U0001F1FD",
            "\U0001F1FE", "\U0001F1FF",
        ]

        for i in range(end):
            await self.client.add_reaction(message, reactions[i])
