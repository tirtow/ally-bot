from ally_exceptions import InsufficentPermissionsError
from command.command import Command
from utils import user_is_officer


class PingCommand(Command):
    """
    Inherits from Command to implement the !signup ping command.

    Provides the implementation for tagging all users who have signed up for
    the trial.
    """

    async def handle(self, message):
        """
        Pings the users who have signed up for the trial in the channel the
        message was sent in.

        message -- the discord.message that was received
        """

        if not user_is_officer(message.author):
            # Must be an officer to run this command
            raise InsufficentPermissionsError()

        await self.trials[message.channel.id].ping()
        msg = message.content.split()[2:]   # Get rid of the !signup ping
        msg = " ".join(msg)

        if len(msg) > 0:
            await self.client.send_message(message.channel, msg)

    def aliases(self):
        """
        Returns a tuple of the aliases for this command.
        """

        return ("ping", "tag", "notify")

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup ping:** ping who has signed up for the trial"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "List and tag all users signed up for a trial.\n" \
            "An optional message can be added at the end to be sent " \
            "after all users are tagged.\n\n" \
            "**This command is restricted to __officers__.**\n\n" \
            "Usage:\n" \
            "- **!signup ping**\n" \
            "- **!signup ping message**: ping and send message"
        await self.send_help_message(channel, msg)
