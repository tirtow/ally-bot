from command.command import Command


class SignupCommand(Command):
    """
    Inherits from Command to implement the !signup <role> command.

    Provides the implementation for signing a user up for a role
    """

    async def handle(self, message):
        """
        Signs the user up for the trial in the channel the message was sent in.

        message -- the discord.Message that was received
        """

        await self.trials[message.channel.id].signup(message)

    def aliases(self):
        """
        Return a tuple of the aliases for this command.
        """

        return ("signup", )

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup role 1, role 2, ...:** signup for the specified role"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Signup for the given roles.\n" \
            "You can signup for multiple roles by providing them in a " \
            "comma-separated list. Certain trials may have restrictions on " \
            "the ranks allowed which can modify the role you signed up " \
            "for.\n\n" \
            "Usage:\n" \
            "- **!signup role 1, role 2, ...**"
        await self.send_help_message(channel, msg)
