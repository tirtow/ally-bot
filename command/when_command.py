from command.command import Command


class WhenCommand(Command):
    """
    Inherits from Command to implement the !signup when command.

    Provides the implementation to print out when a trial's signups open and
    when the trial starts.
    """

    async def handle(self, message):
        """
        Prints out when the given trial's signups will open and when the trial
        will start.

        message -- the discord.Message that was received.
        """

        await self.trials[message.channel.id].when()

    def aliases(self):
        """
        Return a tuple of the aliases for this command
        """

        return ("when", "impatient", "omgwhenwillthisthingstart")

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup when:** list when signups open and trial begins"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Display when signups open and when a trial starts.\n" \
            "Will be sent as two messages where the time displayed on the " \
            "first message is the time local to the user when signups open " \
            "and the time displayed on the second message is the time local " \
            "to the user when the trial starts.\n\n" \
            "Usage:\n" \
            "- **!signup when**"
        await self.send_help_message(channel, msg)
