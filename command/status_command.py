from ally_embed import AllyEmbed
from command.command import Command
from constants import COLORS


class StatusCommand(Command):
    """
    Inherits from Command to implement the !signup status command.

    Provides the implementation for printing the status of opening the signups
    for each scheduled trial.
    """

    async def handle(self, message):
        """
        Provides status information on signups and each trial

        message -- the discord.Message that was received
        """

        color = COLORS["SUCCESS"]
        msg = ""
        for status in (trial.status() for c, trial in self.trials.items()):
            msg += "\n**" + str(status["channel"]) + ":** " + status["status"]
            if status["status"] != "ok":
                color = COLORS["ERROR"]

        embed = AllyEmbed(self.client.user,
                          title="Signup Status",
                          description=msg,
                          color=color)
        await self.client.send_message(message.channel, embed=embed)

    def aliases(self):
        """
        Return a tuple of the aliases of this command
        """

        return ("status", "pulse")

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup status:** get status on signups"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Displays the status of signups opening for trials.\n" \
            "Mainly intended to be a way to easily see which trials failed " \
            "open properly.\n\n" \
            "Usage:\n" \
            "- **!signup status**"
        await self.send_help_message(channel, msg)
