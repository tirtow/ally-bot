from ally_embed import AllyEmbed
from ally_exceptions import InsufficentPermissionsError
from command.command import Command
from constants import COLORS
from utils import user_is_officer


class FixtimestampCommand(Command):
    """
    Inherits from Command to implement the !signup fixtimestamp command

    Handles attempting to automatically correct any trial that failed to open
    correctly by modifying the timestamp until it is in the future.
    """

    async def handle(self, message):
        """
        Handles automatically fixing the signups for all trials.

        message -- the discord.Message that was received.

        Raises InsufficentPermissionsError if user is not an officer
        """

        if not user_is_officer(message.author):
            # Must be an officer to run this command
            raise InsufficentPermissionsError()

        # Fix the signups for each channel
        fixed_channels = []
        for channel_id, trial in self.trials.items():
            if await trial.fix_timestamp():
                fixed_channels.append(trial.channel)

        # Send message with report
        msg = "Completed check, all signups were correct."
        if len(fixed_channels) > 0:
            msg = "Fixed the signups for the following channels:"
            for channel in fixed_channels:
                msg += "\n- " + channel.mention

        embed = AllyEmbed(self.client.user,
                          title="Signup Autocorrect",
                          description=msg,
                          color=COLORS["SUCCESS"])
        await self.client.send_message(message.channel, embed=embed)

    def aliases(self):
        """
        Returns a tuple of the aliases for this command.
        """

        return ("fixtimestamp", "fix_timestamp", "autocorrect", "autoc")

    def description(self):
        """
        Returns the description of this command
        """

        return "**!signup fixtimestamp:** auto-fix signups"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Automatically fix the timestamps for all trials.\n\n" \
            "**This command is restricted to __officers__.\n\n**" \
            "Usage:\n" \
            "- **!signup fixtimestamp**"
        await self.send_help_message(channel, msg)
