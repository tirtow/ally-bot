from command.command import Command


class ListCommand(Command):
    """
    Inherits from Command to implement the !signup list command.

    Provides the implementation for handling listing the users signed up for
    a trial.
    """

    async def handle(self, message):
        """
        Lists the users who have signed up for the trial in the channel the
        message was sent in.

        message -- the discord.Message that was received
        """

        await self.trials[message.channel.id].list()

    def aliases(self):
        """
        Return a tuple of the aliases for ListCommand.
        """

        return ("list", "who")

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup list:** list who has signed up for the trial"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "List the users who have signed up for a trial\n" \
            "If a user has signed up for more than one role, those roles are " \
            "listed next to their name.\n\n" \
            "Usage:\n" \
            "- **!signup list**"
        await self.send_help_message(channel, msg)
