import random

from ally_embed import AllyEmbed
from command.command import Command
from constants import COLORS
from utils import parse_options


class DiceCommand(Command):
    """
    Inherits from Command to implement the !signup dice command.

    Provides the implementation for choosing a random integer.
    """

    async def handle(self, message):
        """
        Chooses a random integer and displays the message.

        message -- the discord.Message that was received
        """

        # Get the min and max value
        min_val = 0
        max_val = 100
        options = parse_options(message)
        if "min" in options:
            min_val = int(options["min"])
        if "max" in options:
            max_val = int(options["max"])

        val = random.randint(min_val, max_val)
        embed = AllyEmbed(self.client.user,
                          title="Dice Roll",
                          description=str(val),
                          color=COLORS["INFO"])
        await self.client.send_message(message.channel, embed=embed)

    def aliases(self):
        """
        Return a tuple of the aliases for this command.
        """

        return ("dice", "diceroll", "roll")

    def description(self):
        return "**!signup dice [min=0] [max=100]:** pick a random number"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Pick a random number between min and max inclusive. Defaults " \
              "to the range [0, 100].\n\n" \
            "Usage:\n" \
            "- **!signup dice**\n"\
            "- **!signup dice min=-100 max=100**"
        await self.send_help_message(channel, msg)
