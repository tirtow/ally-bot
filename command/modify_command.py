from ally_embed import AllyEmbed
from ally_exceptions import InsufficentPermissionsError, MustForceError
from command.command import Command
from constants import COLORS
from utils import user_is_officer, parse_options, get_trial_class


class ModifyCommand(Command):
    """
    Inherits from Command to implement the !signup modify command.

    Provides the implementation to allow an officer to modify attributes of a
    trial.
    """

    async def handle(self, message):
        """
        Handles modifying a trial.

        message -- the discord.Message that was received.

        Raises InsufficentPermissionsError if user is not an officer
        """

        return

        if not user_is_officer(message.author):
            # Must be an officer to run this command
            raise InsufficentPermissionsError()

        # Lowercase the content
        message.content = message.content.lower()

        if self.trials[message.channel.id].signups_open and \
                "-f" not in message.content and \
                "--force" not in message.content:
            # Signups are open and user is not forcing command
            raise MustForceError()

        # Parse options
        options = parse_options(message)

        # Get new type if one given
        trial_class = type(self.trials[message.channel.id])
        if "type" in options:
            trial_class = get_trial_class(options["type"])

        # Initialize the new tiral
        new_trial = trial_class(self.client)
        new_trial.init_trial(self.trials[message.channel.id])

        # Update offset
        if "offset" in options:
            new_trial.offset = int(options["offset"]) * (60 * 60)

        # Unschedule the old trial
        self.trials[message.channel.id].unschedule()

        # Schedule the new trial
        self.trials[message.channel.id] = new_trial
        self.trials[message.channel.id].schedule()

        # Output message
        msg = "Modified trial in %s.\n\n __New attributes:__\n" % \
            (message.channel.mention)
        for k, v in options.items():
            msg += "**%s:** %s\n" % (k, v)

        embed = AllyEmbed(self.client.user,
                          title="Modifying Trial",
                          description=msg,
                          color=COLORS["SUCCESS"])
        await self.client.send_message(message.channel, embed=embed)

    def aliases(self):
        """
        Returns a tuple of the aliases for this command.
        """

        return ("modify", "update", "change")

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup modify [type, offset]:** modify a trial"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Modify a trial that has already been scheduled.\n" \
            "Available options are: type, offset\n\n" \
            "**This command is restricted to __officers__.**\n\n" \
            "Usage:\n" \
            "- **!signup modify [type, offset]**\n" \
            "- **!signup modify [type, offset] --force**: force changes if " \
            "signups are open."
        await self.send_help_message(channel, msg)
