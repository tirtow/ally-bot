import os
from os.path import join

from ally_embed import AllyEmbed
from ally_exceptions import InsufficentPermissionsError
from command.command import Command
from constants import COLORS
from utils import user_is_officer


class UnscheduleCommand(Command):
    """
    Inherits from Command to implement the !signup unschedule command.

    Provides the implementation for deleting an existing Trial.
    """

    async def handle(self, message):
        """
        Handles unscheduling and a trial from the channel message was sent in.

        message -- the discord.Message that was received

        Raise InsufficentPermissionsError if the user is not an officer
        """

        if not user_is_officer(message.author):
            # Must be an officer to run this command
            raise InsufficentPermissionsError()

        self.trials[message.channel.id].unschedule()
        del self.trials[message.channel.id]

        try:
            os.remove(join("trial_info", message.channel.id + ".json"))
        except FileNotFoundError:
            pass

        embed = AllyEmbed(self.client.user,
                          title="Successfully Unscheduled Trial",
                          description="Successfully unscheduled the trial in " +
                                      message.channel.mention,
                          color=COLORS["SUCCESS"])
        await self.client.send_message(message.channel, embed=embed)

    def aliases(self):
        """
        Return a tuple of the aliases for this command.
        """

        return ("unschedule", )

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup unschedule:** delete the signup cycle"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Delete a trial from the channel message was sent in.\n\n" \
            "**This command is restricted to __officers__.**\n\n" \
            "Usage:\n" \
            "- **!signup unschedule**"
        await self.send_help_message(channel, msg)
