from command.command import Command


class DropCommand(Command):
    """
    Inherits from Command to implement the !signup drop command.

    Provides the implementation for removing a from the signups for a trial.
    """

    async def handle(self, message):
        """
        Drops the user from the trial in the channel the message was sent in.

        message -- the discord.Message that was received
        """

        await self.trials[message.channel.id].unsignup(message)

    def aliases(self):
        """
        Return a tuple of the aliases for this command.
        """

        return ("drop", "unsignup", "remove")

    def description(self):
        return "**!signup drop [role 1, role 2, ...]:** drop out of a trial"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Drop out a trial from all roles or the given roles\n\n" \
            "Usage:\n" \
            "- **!signup drop**: drop out of all roles signed up for\n" \
            "- **!signup drop role 1, role 2, ...**: drop out of the given " \
            "roles"
        await self.send_help_message(channel, msg)
