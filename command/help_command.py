from ally_embed import AllyEmbed
from command.command import Command
from constants import COLORS
from utils import get_commands, get_roles


class HelpCommand(Command):
    """
    Inherits Command to implement the !signup help command.

    Provides the implementation to print out messages with help for using
    ally-bot.
    """

    def __init__(self, client, trials):
        """
        Get the available commands.
        """

        super().__init__(client, trials)
        self.commands = None

    async def handle(self, message):
        """
        Prints out the help message for signups to the channel the message
        originated from.

        message -- the discord.Message. Should be !signup help
        """

        # Load in commands if have not already
        if self.commands is None:
            self.commands = get_commands(self.client, self.trials)

        split = message.content.split()
        if len(split) > 2:
            # User specified a chapter of help
            op = split[2]
            if op in ("-a", "--aliases"):
                await self._send_alias_help(message)
            else:
                # Try to send command help message
                for command in self.commands.values():
                    if op in command.aliases():
                        await command.send_help(message.channel)
                        break
                else:
                    # Invalid command
                    await self._send_default_help(message)
        else:
            await self._send_default_help(message)

    def aliases(self):
        """
        Returns a tuple of the aliases for this command.
        """

        return ("help", )

    def description(self):
        """
        Returns the description of this command.
        """

        return "**!signup help:** display this message"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Displays the help messages and command help.\n\n" \
            "Usage:\n" \
            "- **!signup help**: display the help message\n" \
            "- **!signup help -a**: display available aliases\n" \
            "- **!signup help command**: display the help for a command"
        await self.send_help_message(channel, msg)

    async def _send_default_help(self, message):
        """
        Sends the default help message to the channel message was sent in

        message -- the discord.Message that triggered the help message
        """

        msg = "__Available commands:__\n"
        for command in sorted(self.commands.values(), key=lambda c: str(c)):
            msg += "- %s\n" % command.description()

        msg += "\nFor command aliases use: **!signup help --aliases**\n" \
               "For command help use: **!signup help command**\n" \
               "More information: https://tirtow.gitlab.io/ally-bot"
        embed = AllyEmbed(self.client.user,
                          title="Signup Help",
                          description=msg,
                          color=COLORS["INFO"])
        await self.client.send_message(message.channel, embed=embed)

    async def _send_alias_help(self, message):
        """
        Sends the alias help message to the channel message came from

        message -- the discord.Message that triggered the help message
        """

        msg = "__Available command aliases:__\n"
        msg += self._build_alias_help(self.commands.values())
        msg += "\n__Available role aliases:__\n"
        msg += self._build_alias_help(get_roles().values())
        embed = AllyEmbed(self.client.user,
                          title="Signup Help",
                          description=msg,
                          color=COLORS["INFO"])
        await self.client.send_message(message.channel, embed=embed)

    def _build_alias_help(self, alias_list):
        """
        Builds the alias help message for the given list of aliases

        alias_list -- the list of aliases
        """

        result = ""
        for entry in sorted(alias_list, key=lambda e: str(e)):
            if len(entry.aliases()) > 0:
                it = iter(entry.aliases())
                result += "**" + next(it) + ":** "
                count = len(entry.aliases()) - 1
                for alias in it:
                    result += alias
                    if count > 0:
                        result += ", "
                        count -= 1
                result += "\n"
        return result
