from ally_embed import AllyEmbed
from command.command import Command
from constants import COLORS


class AboutCommand(Command):
    """
    Inherits from Command to implement the !signup about command.

    Displays information about the bot.
    """

    async def handle(self, message):
        """
        Displays a message with bot information
        """

        msg = "<@" + self.client.user.id + "> is a Discord bot developed by " \
            "**tirtow** for Tempering Allies to ~~take over the world~~ " \
            "manage signups for weekly trials.\n\n" \
            "More information: https://tirtow.gitlab.io/ally-bot/\n" \
            "Source code: https://gitlab.com/tirtow/ally-bot\n" \
            "More projects by **tirtow**: " \
            "https://tirtow.gitlab.io/"
        embed = AllyEmbed(self.client.user,
                          title="About ally-bot",
                          description=msg,
                          color=COLORS["INFO"])
        await self.client.send_message(message.channel, embed=embed)

    def aliases(self):
        """
        Return a tuple of the aliases for this command.
        """

        return ("about", "info")

    def description(self):
        """
        Returns the description of this command
        """

        return "**!signup about**: get information about ally-bot"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Get information about ally-bot\n\n" \
            "Usage:\n" \
            "- **!signup about**:"
        await self.send_help_message(channel, msg)
