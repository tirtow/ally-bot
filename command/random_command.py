import random

from ally_embed import AllyEmbed
from command.command import Command
from constants import COLORS


TRIAL_NAMES = [
    "Aetherian Archive",
    "Sanctum Ophidia",
    "Hel Ra Citadel",
    "Maw of Lorkhaj",
    "Halls of Fabrication",
    "Asylum Sanctorium",
    "Cloudrest"
]


class RandomCommand(Command):
    """
    Inherits from Command to implement the !signup random command.

    Provides the implementation for choosing a random trial.
    """

    async def handle(self, message):
        """
        Chooses a random trial and displays the message.

        message -- the discord.Message that was received
        """

        i = random.randint(0, len(TRIAL_NAMES) - 1)
        embed = AllyEmbed(self.client.user,
                          title="Random Trial",
                          description=TRIAL_NAMES[i],
                          color=COLORS["INFO"])
        await self.client.send_message(message.channel, embed=embed)

    def aliases(self):
        """
        Return a tuple of the aliases for this command.
        """

        return ("random", "icantchoose", "picktrial")

    def description(self):
        return "**!signup random:** pick a random trial"

    async def send_help(self, channel):
        """
        Sends the help message for this command.

        channel     -- the discord.Channel to send to
        """

        msg = "Get the name of a random trial.\n\n" \
            "Usage:\n" \
            "- **!signup random**"
        await self.send_help_message(channel, msg)
