# ally-bot
A Discord bot to provide welcome messages and handle signups for trials for the
ESO guild Tempering Allies.

### Dependencies
- Python version 3
- discord.py version 0.16.2

## Setup
The makefile command `make setup` will create the `trial_info` folder that is
used to store trial JSONs.

## keys file
For the bot to properly start there should be a file named `keys.py` in the same
directory as the bot. This file should not be checked into version control. The
file should be formatted as follows:

```
keys = {
    "bot_token": <Discord Bot token>,
    "env": <"test" | "production">,
    "server_id": <ID of the Tempering Allies server>,
    "tirtow_id": <ID for tirtow>,
    "channels": {
        "welcome": <ID>,
        "questions": <ID>,
        "invite": <ID>,
        "anonymous": <ID>,
        "vet-reqs": <ID>
    }
}
```

## Running
1. Create a Discord application with a bot user and get the token. [link](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token)
2. Copy the token into the `keys.py` file.
3. Run the bot using `python3 -u ally_bot.py`

## Running Production
The bot is running as a systemctl service defined in
`/etc/systemd/system/ally.service`.

The bot can be started, stopped, and restarted by using:
```
$ sudo systemctl start ally.service
$ sudo systemctl stop ally.service
$ sudo systemctl restart ally.service
```

And the bot's logs can be viewed using:
```
$ sudo journalctl -u ally
```
