# Colors for Embeds
COLORS = {
    "SUCCESS": 0x269B42,
    "ERROR": 0xF44242,
    "INFO": 0x4286F4
}

# Supported timezones for initializing trials
TIMEZONES = {
    "UTC": 0,
    "EDT": -4,
    "EST": -5,
    "CDT": -5,
    "CST": -6,
    "MDT": -6,
    "MST": -7,
    "PDT": -7,
    "PST": -8
}

# Seconds to wait after closing before clearing
CLEAR_DELAY_SECONDS = 30 * 60
