from ally_exceptions import InvalidRankError
from trial.trial import Trial


# Ranks allowed to run veteran trials
ALLOWED_RANKS = {
    "Embroidered": "461644209213145114",
    "Totally Turpen": "423307280914579457",
    "Imposing Iridium": "461644013192085519",
    "Ally": "508257820970450954"
}


class VeteranTrial(Trial):
    """
    Represents a veteran ESO trial.

    Manages the signups, opening, and closing of a veteran trial in a single
    discord channel. Checks the rank of a player when they sign up.
    """

    def __init__(self, client, roles=("tank_role", "healer_role", "melee_role",
                                      "ranged_role")):
        """
        Passes control to Trial to initialize this VeteranTrial.

        client  -- the client running the bot
        roles   -- the roles. (default ("TANK", "HEALER", "MELEE", "RANGED"))
        """

        super().__init__(client, roles)
        self.type = "VeteranTrial"

    async def signup(self, message):
        """
        Overrides signup() to check the rank of the user.

        Passes control to Trial::signup() if the user is one of the veteran
        ranks in ALLOWED_RANKS.

        message     -- the discord.Message that was received.

        Raises InvalidRoleError if the user is not a veteran rank
        """

        is_allowed = False
        roles = set(map(lambda r: r.id, message.author.roles))
        for name, rank_id in ALLOWED_RANKS.items():
            if rank_id in roles:
                is_allowed = True
                break

        if not is_allowed:
            raise InvalidRankError()
        await super().signup(message)
