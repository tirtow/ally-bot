import asyncio
import time

from trial.veteran_trial import VeteranTrial
from utils import log


OPEN_ALL_OFFSET = 2 * 24 * 60 * 60   # Open to all ranks 2 days before trial


class ProgressionTrial(VeteranTrial):
    """
    Represents a progression ESO trial.

    Manages the signups, opening, and closing of a progression trial in a
    single discord channel. Progression trials open signups to all vet-ready
    ranks two days before the trial is to begin.
    """

    def __init__(self, client, roles=("tank_role", "healer_role", "melee_role",
                                      "ranged_role")):
        """
        Passes control to Trial to initialize this ProgressionTrial.

        client  -- the client running the bot
        roles   -- the roles.
        """

        super().__init__(client, roles)
        self.type = "ProgressionTrial"
        self.signups_open_all = False
        self.tasks["open_all"] = None

    async def signup(self, message):
        """
        Overries signup() to check the rank of the user.

        If the user is not an Iridium, appends "Backup" to their roles then
        passes control to VeteranTrial::signup()

        message     -- the discord.Message that was received
        """

        roles = set(map(lambda r: r.id, message.author.roles))
        if not self.signups_open_all and self.allowed_id not in roles:
            # Get list of roles
            split = message.content.split()[1:]
            roles = " ".join(split)
            roles = roles.split(",")

            # Add "backup" to each role
            roles = ",".join(map(self._add_backup, roles))
            message.content = "!signup " + roles

        await super().signup(message)

    def _add_backup(self, role):
        """
        Appends the string "backup" to role if not present
        """

        if "backup" not in role:
            return role + " backup"
        return role

    def schedule(self, *, force_open_msg=False):
        """
        Overrides schedule() to schedule the tasks to open signups to all
        vet-ready users
        """

        super().schedule(force_open_msg=force_open_msg)

        # Schedule open for all users
        if self.tasks["open_all"] is None:
            self.tasks["open_all"] = asyncio.ensure_future(
                    self._schedule_signup_open_all(force_open_msg))

    async def _schedule_signup_open_all(self, force_open_msg):
        """
        Schedules the opening of signups to all vet-ready users

        force_open_msg -- whether or not to force the open message.
        """

        secs_to_wait = self.timestamp - OPEN_ALL_OFFSET - time.time()
        ping = force_open_msg
        if secs_to_wait > 0:
            await asyncio.sleep(secs_to_wait)
            ping = True

        if not self.signups_open_all:
            log("Opening signups for all vet players for " + str(self.channel))
            self.signups_open_all = True

            # Convert backup roles
            log("converting backups")
            self._convert_backups()

            if ping:
                msg = "**Trial start:** " + self._time_until(self.timestamp)
                text = "<@&461644013192085519>, <@&423307280914579457>, " \
                       "<@&461644209213145114> and <@&508257820970450954> " \
                       "you may now signup!"
                await self._send_when_embed("Trial starts", msg, self.timestamp,
                                            text)

    def _convert_backups(self):
        """
        Converts all backup roles to the actual role.
        """

        for role in self.signups:
            if role.is_backup():
                to_role = self._get_to_role(role.name)
                if to_role is not None:
                    for _, member in role.signups.items():
                        to_role.signup(member)
                    role.clear()
                    self._write()

    def _get_to_role(self, name):
        """
        Gets the role to convert to

        name    -- the name of the role converting from. Should start with
                   "Backup"

        Returns the role or None if not found
        """

        name = " ".join(name.split(" ")[1:])
        for role in self.signups:
            if role.name == name:
                return role

        return None
