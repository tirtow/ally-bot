"""
The trials available for ally-bot to use.

Trials are loaded in at runtime whenever a new Trial is instantiated.

Trials must follow the naming structure:
- filename:   somename_trial.py
- class name: SomenameTrial.py
Trial filenames may only contain a single "_" and the class names should only
capitalize the first letter and "Trial".

Trials **must** respond to the methods that `Trial` responds to in order to
integrate properly with ally-bot. See `help(Trial)` for more information.
"""
