import asyncio
from datetime import datetime, timedelta
import importlib
import json
from os.path import join, isfile
import pandas as pd
import time

from aliases import DAYS
from ally_embed import AllyEmbed, WhenEmbed
from ally_exceptions import SignupsClosedError, InvalidCommandError, \
    InvalidStartTimeError, InvalidRoleError
from constants import COLORS, TIMEZONES, CLEAR_DELAY_SECONDS
from keys import keys
from utils import to_timestamp, log, get_roles


class Trial:
    """
    Defines the implementation needed for a Trial along with default methods.

    Manages the signups, opening, and closing of trials in a single discord
    channel.
    """

    def __init__(self, client, roles=()):
        self.client = client
        self.server = self._get_server()

        # Booleans to determine state of this Trial
        self.signups_open = False
        self.signups_cleared = True

        # Build the lists of signed up users
        self.signups = []
        for role in roles:
            self.signups.append(self._get_role_instance(role))

        # The tasks for this Trial that have been scheduled
        self.tasks = {
            "open":  None,
            "close": None,
            "clear": None
        }

        log("Constructed new Trial")

    def init_message(self, message, options):
        """
        Initializes the start time for this trial using the message that was
        sent.

        message     -- the discord.Message received
        options     -- the dict of options from message

        Raises InvalidCommandError if an invalid timezone is given
        Raise InvalidStartTimeError if the start time is invalid
        """

        # Set the channel
        self.channel = message.channel

        # Split to everything after !signup init
        split = message.content.lower().split()[2:]
        if len(split) == 0:
            raise InvalidCommandError()

        # Get the target start day
        tz_offset = self._get_timezone_offset(options)
        start = self._get_start_day(split[0], options, tz_offset)

        #  Chanage start time based on user options
        time_options = self._get_start_time_options(start, options, tz_offset)
        inc = 0
        if "day" in time_options:
            inc = time_options["day"] - start.day
            del time_options["day"]
        start_time = start.replace(**time_options)
        start_time += timedelta(days=inc)

        try:
            # Set the offset
            self.offset = int(options["offset"]) * (60 * 60)
        except ValueError:
            raise InvalidStartTimeError()

        # Verify not starting in past
        if (start_time - datetime.now()).total_seconds() < 0:
            raise InvalidStartTimeError()

        # Get the timestamp
        if type(start_time) == int:
            self.timestamp = start_time
        else:
            self.timestamp = to_timestamp(start_time)
        self._write()

    def init_legacy(self, options):
        """
        Initializes the start time for this trial using the legacy method where
        the user gives the exact UTC date and time for the first instance of
        the Trial.

        options -- the dict of options to initialize with

        Raise InvalidStartTimeError if the start time is in the past
        """

        # Get the start time
        start_time = datetime(year=int(options["year"]),
                              month=int(options["month"]),
                              day=int(options["day"]),
                              hour=int(options["hour"]),
                              minute=int(options["minute"]))

        # Verify not starting in past
        if (start_time - datetime.now()).total_seconds() < 0:
            raise InvalidStartTimeError()

        if type(start_time) == int:
            self.timestamp = start_time
        else:
            self.timestamp = to_timestamp(start_time)
        self._write()

    def init_file(self, entry):
        """
        Initializes this Trial from the file.

        entry   -- the dict entry with the trial info
        """

        self.channel = self.client.get_channel(entry["channel_id"])
        self.offset = entry["offset"]

        # Set the timestamp and ensure it's in the future
        self.timestamp = entry["timestamp"]
        while self.timestamp < time.time():
            self.timestamp += (7 * 24 * 60 * 60)

        # Build the signup lists
        for role in self.signups:
            role.load(entry[role.class_name()], self.server)

    def init_trial(self, other):
        """
        Initializes this Trial from another Trial.

        other   -- the Trial to initialize from
        """

        self.channel = other.channel
        self.offset = other.offset
        self.timestamp = other.timestamp
        self._write()

    async def signup(self, message, order=None):
        """
        Attempts to sign a user up for various roles for the Trial

        message -- the discord.Message that was received. Should contain a comma
                   separated list of roles to signup for.
        order   -- the order in the signups list for the member. If None
                   defaults to the end of the list. (default None)

        Raises SignupsClosedError if signups have not been opened yet.
        Raises InvalidCommandError if given no valid roles """

        # Check that signups have been opened
        if not self.signups_open:
            raise SignupsClosedError()

        # Get set of roles
        split = message.content.lower().split()[1:]
        roles = set(" ".join(split).split(","))

        # Sign user up for roles
        success_roles = []  # List of roles successfully signed up for
        failed_roles = []   # List of roles failed to signup for
        self._signup_for_roles(roles, message.author, success_roles,
                               failed_roles, order)

        # Check actually tried to signup for a role
        if len(success_roles) == 0 and len(failed_roles) == 0:
            raise InvalidRoleError()

        # Write out file
        self._write()

        # Send message with result of signup attempt
        await self._send_signup_msg(message, success_roles, failed_roles)

        log("Signed user " + str(message.author) + " up for the roles " +
            str(success_roles))

    async def unsignup(self, message):
        """
        Removes a user from any role they signed up for.

        Accepts a list of the roles to drop as a comma-separated list. If no
        role is provided, drops the user from all roles they have signed up for.
        If any roles are provided, drops the user from only those roles.

        message -- the discord.Message to handle

        Raises SignupsClosedError if signups have not opened yet
        """

        # Check that signups have been opened
        if not self.signups_open:
            raise SignupsClosedError()

        # Get set of roles to drop from
        split = message.content.lower().split()[2:]
        roles = set("".join(split).split(","))
        if len(split) == 0:
            # User wants to drop out of all roles
            roles = set(map(lambda s: s.aliases()[0], self.signups))

        # Drop user from roles
        success_roles = self._drop_from_roles(roles, message.author)

        # Write out file
        self._write()

        # Send message
        await self._signup_drop_msg(message, roles, success_roles)

        log("Dropped user " + str(message.author) + " from the roles " +
            str(success_roles))

    async def list(self):
        """
        Sends the list of users who have signed up for this trial.

        Raises SignupsClosedError if the signups have been cleared
        """

        if self.signups_cleared:
            raise SignupsClosedError()

        # Build the message
        user_roles = self._map_user_to_roles()
        msg = "**Type:** %s\n\n" % self.trial_type()
        for role in self.signups:
            s = role.list(user_roles)
            if len(s) > 0:
                msg += s + "\n"
        msg += "\n**Unique signups:** %d" % len(user_roles)

        # Print out message
        embed = AllyEmbed(self.client.user,
                          title="Signups",
                          description=msg,
                          color=COLORS["SUCCESS"])
        await self.client.send_message(self.channel, embed=embed)

    async def ping(self):
        """
        Pings the list of users who have signed up for this trial.

        Raises SignupsClosedError if the signups have been cleared
        """

        if self.signups_cleared:
            raise SignupsClosedError()

        # Build the message
        user_roles = self._map_user_to_roles()
        msg = ""
        for role in self.signups:
            msg += role.ping(user_roles) + "\n"
        msg += "\n**Unique signups:** %d" % len(user_roles)

        # Print out message
        await self.client.send_message(self.channel, msg)

    async def when(self):
        """
        Sends the message with how long until signups open and how long until
        the trial starts.
        """

        # Send when signups open
        msg = "**Signups:** "
        if self.signups_open:
            msg += "Open!"
        else:
            msg += self._time_until(self.timestamp - self.offset)
        await self._send_when_embed("Signups open", msg,
                                    self.timestamp - self.offset)

        # Send when trial starts
        msg = "**Trial start:** " + self._time_until(self.timestamp)
        await self._send_when_embed("Trial starts", msg, self.timestamp)

    def schedule(self, *, force_open_msg=False):
        """
        Schedules all of the tasks needed for this Trial synchronizing them with
        each other.

        force_open_msg -- whether or not to force the open message. Default is
                          False
        """

        if self.timestamp < time.time():
            log("Trial for " + str(self.channel) + " has timestamp in past. " +
                "Rescheduling it")
        else:
            log("Scheduling tasks for " + str(self.channel) + ". Start " +
                "timestamp is " + str(self.timestamp))

            # Signups open task
            if self.tasks["open"] is None:
                self.tasks["open"] = asyncio.ensure_future(
                    self._schedule_signup_open(force_open_msg))

            # Signups close task
            if self.tasks["close"] is None:
                self.tasks["close"] = asyncio.ensure_future(
                    self._schedule_signup_close())

            # Signups clear task
            if self.tasks["clear"] is None:
                self.tasks["clear"] = asyncio.ensure_future(
                    self._schedule_signup_clear())

    def unschedule(self):
        """
        Unschedules the tasks for this Trial
        """

        for name, task in self.tasks.items():
            if task is not None:
                task.cancel()
                self.tasks[name] = None

    def status(self):
        """
        Gets whether or not this trial opened correctly
        """

        if self.signups_open == self._signups_should_be_open():
            return {"channel": self.channel, "status": "ok"}
        return {"channel": self.channel, "status": "error"}

    async def fix_timestamp(self):
        """
        Fixes the timestamp and reschedules this Trial if it failed to open.

        Returns True if had to reschedule, False otherwise
        """

        if self._signups_should_be_open() and not self.signups_open:
            # Signups did not open properly

            # Unschedule any tasks
            self.unschedule()

            # Correct the timestamp if needed
            now = time.time()
            while self.timestamp < now:
                self.timestamp += (7 * 24 * 60 * 60)

            # Reschedule the trial
            self.schedule(force_open_msg=True)

            log("Corrected signups for " + str(self.channel))
            return True
        return False

    def to_dict(self):
        """
        Gets the dict representation of this Trial
        """

        # Build result
        result = {
            "channel_id": self.channel.id,
            "timestamp": self.timestamp,
            "offset": self.offset,
            "type": self.type,
        }

        # Add the lists of signups
        for role in self.signups:
            result[role.class_name()] = role.to_dict()

        return result

    def roles(self):
        """
        Gets a list of the names of the roles for this trial
        """

        return [role.aliases()[0] for role in self.signups]

    def trial_type(self):
        """
        Return the type of this Trial in human readable format
        """

        return self.type[:-5] + " " + self.type[-5:]

    def __str__(self):
        """
        Builds a string describing this Trial
        """

        return "channel=%s, timestamp=%d, offset=%d" % \
               (str(self.channel), self.timestamp, self.offset)

    async def _schedule_signup_open(self, force_open_msg):
        """
        Schedules the opening of signups for this Trial

        force_open_msg -- whether or not to force the open message.
        """

        secs_to_wait = self.timestamp - self.offset - time.time()
        ping = force_open_msg
        if secs_to_wait > 0:
            await asyncio.sleep(secs_to_wait)
            ping = True

        if not self.signups_open:
            log("Opening signups for " + str(self.channel))
            self.signups_open = True
            self.signups_cleared = False

            if ping:
                msg = "**Trial start:** " + self._time_until(self.timestamp)
                await self._send_when_embed("Trial starts", msg, self.timestamp,
                                            "@everyone Signups now open!")

    async def _schedule_signup_close(self):
        """
        Schedules the closing of signups for this Trial
        """

        secs_to_wait = self.timestamp - time.time()
        if secs_to_wait > 0:
            await asyncio.sleep(secs_to_wait)

        if self.signups_open:
            log("Closing signups for " + str(self.channel))
            self.signups_open = False

            # Write csv
            self._write_to_csv()

            embed = AllyEmbed(self.client.user,
                              title="Signups are now closed",
                              description="Signups are now closed for " +
                                          self.channel.mention,
                              color=COLORS["INFO"])
            await self.client.send_message(self.channel, embed=embed)
            await self.list()

    async def _schedule_signup_clear(self):
        """
        Schedules clearing of this Trial
        """

        secs_to_wait = self.timestamp - time.time() + CLEAR_DELAY_SECONDS
        if secs_to_wait > 0:
            await asyncio.sleep(secs_to_wait)

        if not self.signups_cleared:
            log("Clearing signups for " + str(self.channel))

            # Do clearing
            self.signups_cleared = True
            self.signups_open = False
            self.timestamp += (7 * 24 * 60 * 60)
            for role in self.signups:
                role.clear()

            # Ensure no tasks are scheduled
            self.unschedule()

            # Schedule new tasks
            self.schedule()

            self._write()

    def _list_signup_roles(self, l):
        """
        Lists out the roles from l into a single string

        l -- the list to output

        Returns the string
        """

        result = ""

        # Properly pluralize "role"
        result += self._pluralize(len(l), "role") + " "

        # List each role with a comma
        for role in l:
            result += role + ", "

        # Return the result minus the last comma if had any items
        if len(l) > 0:
            return result[:-2]
        return result

    def _map_user_to_roles(self):
        """
        Maps each user to a list of the roles they signed up for.

        Returns a dict mapping each user to their roles
        """

        result = {}
        for role in self.signups:
            for user in role.signups.values():
                if user not in result:
                    result[user] = []
                result[user].append(role.abbr())
        return result

    def _time_until(self, timestamp):
        """
        Gets the string showing the time until the given timestamp

        timestamp -- the timestamp

        Returns the time until string
        """

        diff = int(round(timestamp - time.time()))
        seconds = diff % 60
        diff //= 60
        minutes = diff % 60
        diff //= 60
        hours = diff % 24
        diff //= 24

        return str(diff) + self._pluralize(diff, " day") + ", " + \
            str(hours) + self._pluralize(hours, " hour") + ", " + \
            str(minutes) + self._pluralize(minutes, " minute") + ", " + \
            str(seconds) + self._pluralize(seconds, " second")

    def _pluralize(self, amount, base):
        """
        Provides a simple pluralization for the given base if amount is not 1

        base -- the string to pluralize

        Returns base if amount is 1, base + "s" otherwise
        """

        if amount == 1:
            return base
        return base + "s"

    async def _send_when_embed(self, footer_text, msg, timestamp, message=None):
        """
        Handles sending a WhenEmbed

        footer_text     -- the text for the footer
        msg             -- the message to send
        timestamp       -- the timestamp
        message         -- optional message to send. Default is None
        """

        embed = WhenEmbed(footer_text,
                          title="Wait times for " + self.channel.name,
                          description=msg,
                          color=COLORS["INFO"],
                          timestamp=datetime.fromtimestamp(timestamp))
        if message is not None:
            await self.client.send_message(self.channel, message, embed=embed)
        else:
            await self.client.send_message(self.channel, embed=embed)

    def _write(self):
        """
        Writes out this trial to a JSON
        """

        with open(join("trial_info", self.channel.id + ".json"), "w") as f:
            f.write(json.dumps(self.to_dict()))

    def _signups_should_be_open(self):
        """
        Gets whether or not signups for this Trial should be open

        Returns True if signups should be open, False otherwise
        """

        return self.timestamp - self.offset < time.time()

    def _get_day_of_week(self, day):
        """
        Gets the integer value for the day of the week.

        day -- the string of the day to get

        Returns the integer value if found, None if an invalid day.
        """

        day = day.lower()
        for num, vals in DAYS.items():
            if day in vals:
                return num
        return None

    def _get_timezone_offset(self, options):
        """
        Gets the timezone offset from UTC from options.

        options     -- the dict of options given by the user.

        Returns the timezone offset if provided defaulting to EDT if none given.

        Raises InvalidCommandError if a timezone not present in TIMEZONES is
        given.
        """

        if "tz" in options and options["tz"].upper() in TIMEZONES:
            # User specified a timezone
            return TIMEZONES[options["tz"].upper()]
        elif "tz" in options:
            # User specified an invalid timezone
            raise InvalidCommandError()

        # Default to EDT
        return TIMEZONES["EST"]

    def _get_start_day(self, day, options, tz_offset):
        """
        Gets the next instance of the target day to start on.

        day         -- the day of the week to get
        options     -- the dict of options given by the user.
        tz_offset   -- the timezone offset.

        Returns the datetime.datetime of the next instance of day.

        Raises InvalidStartTimeError if an invalid day of the week is given
        """

        # Get the start day skipping the current day if in options
        start = datetime.utcnow() + timedelta(hours=tz_offset)
        if options["skip"] == "true":
            start += timedelta(days=1)

        # Get the target start day
        target = self._get_day_of_week(day)
        if target is None:
            raise InvalidStartTimeError()

        # Get the next instance of the target day
        while start.weekday() != target:
            start += timedelta(days=1)

        return start

    def _get_start_time_options(self, start, options, tz_offset):
        """
        Gets the start time options for this Trial.

        start       -- the datetime.datetime of the first day of the trial.
        options     -- the dict of options provided by the user.
        tz_offset   -- the timezone offset.

        Returns a dict of the time options

        Raises InvalidStartTimeError if any of the options are not integers.
        """

        try:
            # Get the time to start
            time_options = {}
            time_options["hour"] = (int(options["hour"]) - tz_offset) % 24

            # Increment the day if pass midnight when converting hour
            if int(options["hour"]) - tz_offset >= 24:
                time_options["day"] = start.day + 1

            for name, val in options.items():
                if name in ("minute", "second", "microsecond"):
                    time_options[name] = int(val)

            return time_options
        except ValueError:
            raise InvalidStartTimeError()

    def _get_server(self):
        """
        Gets the Tempering Allies discord.Server

        Returns the server, None if cannot find it.
        """

        for server in self.client.servers:
            if server.id == keys["server_id"]:
                return server
        return None

    def _get_role_instance(self, role):
        """
        Returns an instance of the given role.

        role    -- the role to get an instance of

        Returns the instance.
        """

        split = role.lower().split("_")

        # Import the module
        module = importlib.import_module("role." + "_".join(split))

        # Create instance of class
        name = "".join(map(lambda s: s.title(), split))
        return getattr(module, name)()

    def _write_to_csv(self):
        """
        Writes out info on this trial to csv
        """

        columns = list(map(lambda r: str(r), get_roles().values()))
        columns += ["day_of_week", "date", "unique"]
        result = {key: 0 for key in sorted(columns)}

        # Add the date
        result["date"] = datetime.utcnow().strftime("%Y%m%d%H%M")
        result["day_of_week"] = datetime.utcnow().strftime("%A")

        # Add unique signups
        result["unique"] = len(self._map_user_to_roles())

        # Add the roles
        for role in self.signups:
            result[str(role)] = len(role)

        # Write out csv
        df = pd.DataFrame(result, index=[0])
        if isfile("csv/trial_stats.csv"):
            df.to_csv("csv/trial_stats.csv", mode="a", header=False)
        else:
            df.to_csv("csv/trial_stats.csv", mode="a")

    def _signup_for_roles(self, roles, member, success, failed, order):
        """
        Attempts to sign member up for each role in roles.

        Appends all roles successfully signed up for to success and all roles
        failed to signup for to failed.

        roles       -- an iterable of the roles to signup for
        member      -- the discord.Member to signup
        success     -- the list to append successes to
        failed      -- the list to append fails to
        order       -- the order to insert the user in
        """

        # Loop through each role and attempt to sign the user up for that role
        for role_name in roles:
            role_name = role_name.strip()

            # Iterate through possible roles
            for role in self.signups:
                if role_name in role.aliases():
                    # Sign user up for role
                    if role.signup(member, order):
                        success.append(str(role))
                    else:
                        failed.append(str(role))
                    break

    async def _send_signup_msg(self, message, success_roles, failed_roles):
        """
        Send the message with the result of the signup attempt.

        message         -- the discord.Message received
        success_roles   -- an iterable of the roles signed up for
        failed_roles    -- an iterable of the roles failed to signup for
        """

        # Build the message
        msg = self._signup_success_msg(success_roles, message.author) + \
            self._signup_failed_msg(failed_roles, message.author)

        # Output the message
        embed = AllyEmbed(self.client.user,
                          title="Signup for trial",
                          description=msg,
                          color=COLORS["SUCCESS"])
        await self.client.send_message(message.channel, embed=embed)

    def _signup_success_msg(self, success_roles, member):
        """
        Builds the message to send with the roles the user successfully signed
        up for.

        success_roles   -- an iterable of the roles signed up for
        member          -- the discord.Member

        Returns the message string
        """

        msg = ""
        if len(success_roles) > 0:
            # List roles signed up for successfully
            msg = "Signed <@" + member.id + "> up for the "
            msg += self._list_signup_roles(success_roles) + "\n\n"

        return msg

    def _signup_failed_msg(self, failed_roles, member):
        """
        Builds the message to send with the roles the user failed to sign up for

        failed_roles    -- an iterable of the roles failed to sign up for
        member          -- the discord.Member

        Returns the message string
        """

        msg = ""
        if len(failed_roles) > 0:
            # List roles failed to signup for
            msg += "<@" + member.id + "> was already signed up for the "
            msg += self._list_signup_roles(failed_roles)

        return msg

    def _drop_from_roles(self, roles, member):
        """
        Attempts to drop member from the given roles.

        roles   -- an iterable of the roles to drop from
        member  -- the discord.Member to drop

        Returns a list of the roles dropped from
        """

        success_roles = []

        # Drop the user from the specified roles
        for role_name in roles:
            for role in self.signups:
                if role_name in role.aliases():
                    # Remove user from role
                    if role.unsignup(member):
                        success_roles.append(role_name.lower())
                    break

        return success_roles

    async def _signup_drop_msg(self, message, roles, success_roles):
        """
        Send the message on whether or not managed to drop user from roles

        message         -- the discord.Message received
        roles           -- the list of roles to drop from
        success_roles   -- the list of roles actually dropped from
        """

        if len(success_roles) == 0:
            # Failed to drop any roles
            await self._send_not_signed_up_msg(message, roles)
        else:
            # Dropped from roles
            await self._send_drop_msg(message, success_roles)

    async def _send_not_signed_up_msg(self, message, roles):
        """
        Tell the user they were not signed up for any of the given roles

        message     -- the discord.Message received
        roles       -- an iterable of the roles
        """

        msg = "You were not signed up for "
        if len(roles) == 1:
            msg += "the role "
        else:
            msg += "any of the roles "
        msg += ", ".join(roles)

        embed = AllyEmbed(self.client.user,
                          title="Failed to drop out of trial",
                          description=msg,
                          color=COLORS["ERROR"])
        await self.client.send_message(message.channel, embed=embed)

    async def _send_drop_msg(self, message, success_roles):
        """
        Tell the user which roles they were dropped from.

        message         -- the discord.Message received
        success_roles   -- the roles dropped from
        """

        msg = "You were dropped from the "
        msg += self._list_signup_roles(success_roles)
        embed = AllyEmbed(self.client.user,
                          title="Dropped out of trial",
                          description=msg,
                          color=COLORS["SUCCESS"])
        await self.client.send_message(message.channel, embed=embed)
