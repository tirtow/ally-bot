from trial.trial import Trial


class NormalTrial(Trial):
    """
    Represents a normal ESO trial.

    Manages the signups, opening, and closing of a normal trial in a single
    discord channel. Agnostic to the rank of a player when they sign up.
    """

    def __init__(self, client, roles=("tank_role", "healer_role", "dps_role")):
        """
        Passes control to Trial to initialize this NormalTrial.

        client  -- the client running the bot
        roles   -- the roles. (default ("TankRole", "HealerRole", "DpsRole"))
        """

        super().__init__(client, roles)
        self.type = "NormalTrial"
