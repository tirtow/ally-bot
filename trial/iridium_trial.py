from trial.progression_trial import ProgressionTrial


class IridiumTrial(ProgressionTrial):
    """
    Represents an Iridium team trial ESO trial.

    Manages the signups, opening, and closing of an Iridium trial in a single
    discord channel. Checks the rank of a player when they sign up.
    """

    def __init__(self, client, roles=("tank_role", "healer_role", "melee_role",
                                      "ranged_role", "backup_tank_role",
                                      "backup_healer_role", "backup_melee_role",
                                      "backup_ranged_role")):
        """
        Passes control to ProgressionTrial to initialize this IridiumTrial.

        client  -- the client running the bot
        roles   -- the roles.
        """

        super().__init__(client, roles)
        self.type = "IridiumTrial"
        self.allowed_id = "461644013192085519"
