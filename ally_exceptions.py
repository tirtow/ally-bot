class SignupsClosedError(Exception):
    """
    Thrown to signal the user attempt to use a command on a trial that requires
    signups to be open for that trial when signups were not open.
    """


class InvalidCommandError(Exception):
    """
    Thrown to signal the user gave an invalid command.
    """


class InvalidPollError(Exception):
    """
    Thrown to signal the user gave an invalid command to create a poll.
    """


class TrialAlreadyScheduledError(Exception):
    """
    Thrown to signal the user attempted to create a trial in a channel where
    a trial has already been scheduled
    """


class InvalidStartTimeError(Exception):
    """
    Thrown to signal the user gave an invalid start time when creating a trial.
    """


class InvalidTrialTypeError(Exception):
    """
    Thrown to signal the user gave an invalid trial type
    """


class InsufficentPermissionsError(Exception):
    """
    Thrown to signal the user does not have permission to use the command
    """


class InvalidRankError(Exception):
    """
    Thrown to signal the suer is not a veteran ranked player
    """


class InvalidRoleError(Exception):
    """
    Thrown to signal the user entered an invalid role when signing up
    """


class MustForceError(Exception):
    """
    Thrown to signal the user must force the command
    """


class InvalidRowError(Exception):
    """
    Thrown to signal the user chose an invalid row
    """


class InvalidColumnError(Exception):
    """
    Thrown to signal the user chose an invalid column
    """


class SpotAlreadyTakenError(Exception):
    """
    Thrown to signal the spot has already been taken
    """
