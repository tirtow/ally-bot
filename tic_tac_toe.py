import random

from ally_exceptions import InvalidRowError, InvalidColumnError, \
    SpotAlreadyTakenError


class TicTacToe:
    """
    Manages a game of Tic-Tac-Toe between the computer and the user.
    """

    def __init__(self, client, channel, rows=3, cols=3):
        """
        Creates a new Tic-Tac-Toe game.

        client      -- the client running the bot
        channel     -- the discord.Channel playing in
        rows        -- the number of rows. (default 3)
        cols        -- the number of columns. (default 3)
        """

        self.client = client
        self.player_turn = True
        self.channel = channel
        self.rows = rows
        self.cols = cols

        # Initiate the board
        self.board = []
        for i in range(self.rows + 2):
            self.board.append([" " for i in range(self.cols + 2)])

    async def prompt_user(self):
        """
        Prints the board and prompts the user to enter their next move.
        """

        msg = str(self) + "\n**It is your turn!**\n" \
            "Place a piece with `!signup tictactoe row col`"
        await self.client.send_message(self.channel, msg)

    async def print_winner(self, winner):
        """
        Prints the board and the winner.

        winner  -- the name of the winner
        """

        msg = str(self) + "\n**Game over, %s won!**" % winner
        await self.client.send_message(self.channel, msg)

    def computer_turn(self):
        """
        Takes the computer's turn.

        Searches for locations where the computer can win or user can win to
        place. If not randomly selects an open spot.

        Returns whether or not the computer won, None if no spots to place.
        """

        for entry in self._placement_options():
            if entry is not None:
                return self.place(entry[0], entry[1], "O")
        return None

    def place(self, row, col, piece):
        """
        Places a piece in the given row and column

        row     -- the row to place in. 1 <= row <= self.rows
        col     -- the column to place in. 1 <= col <= self.cols
        piece   -- the character to place

        Returns True if the placement resulted in a win condition, False
        otherwise.

        Raises InvalidRowError if the row is invalid
        Raises InvalidColumnError if the column is invalid
        Raises SpotAlreadyTakenError if the row, col is already taken
        """

        # Try to convert the row and column
        try:
            row = int(row)
        except ValueError:
            raise InvalidRowError()
        try:
            col = int(col)
        except ValueError:
            raise InvalidColumnError()

        # Check valid location
        if row < 1 or row > self.rows:
            raise InvalidRowError()
        if col < 1 or col > self.cols:
            raise InvalidColumnError()
        if self.board[row][col] != " ":
            # Spot has already been taken
            raise SpotAlreadyTakenError()

        # Place the piece
        self.board[row][col] = piece

        # Return if won
        return self._is_winner(row, col, piece)

    def _placement_options(self):
        """
        Gets a list of the placement options for the computer.

        Orders the options by locations that result in a computer win, locations
        that result in a user win, randomly ordered open spots.

        Returns the list of options
        """

        open_spots = self._open_spots()
        result = []

        # Places computer can win
        result += self._winning_spots(open_spots, "O")

        # Places user can win
        result += self._winning_spots(open_spots, "X")

        # Randomize open spots
        random.shuffle(open_spots)
        result += open_spots

        return result

    def _open_spots(self):
        """
        Returns a list of all the open spots on the board.
        """

        result = []
        for r in range(1, self.rows + 1):
            for c in range(1, self.cols + 1):
                if self.board[r][c] == " ":
                    result.append((r, c))

        return result

    def _winning_spots(self, open_spots, piece):
        """
        Gets a list of open spots that result in a win for the given piece.

        open_spots  -- an iterable of the open spots on the board
        piece       -- the piece to place

        Returns a list of the spots where placing piece results in a win
        condition
        """

        result = []
        for r, c in open_spots:
            self.board[r][c] = piece
            if self._is_winner(r, c, piece):
                # Found winning spot
                result.append((r, c))

            # Reset board
            self.board[r][c] = " "

        return result

    def _is_winner(self, row, col, piece):
        """
        Gets whether or not the piece that was placed resulted in a win.

        row     -- the row where the piece was placed
        col     -- the column where the piece was placed
        piece   -- the piece that was placed

        Returns True if resulted in a win condition, False otherwise
        """

        # Get the row, column, and diagonals of the placed piece
        sets = (
            self.board[row][1:self.rows + 1],
            [self.board[r][col] for r in range(1, self.rows + 1)],
            self._down_diagonal(row, col),
            self._up_diagonal(row, col)
        )

        # Check if one of the sets only has one piece
        for s in sets:
            if len(s) == 3 and all(map(lambda c: c == piece, s)):
                return True

        # No winning condition
        return False

    def _up_diagonal(self, row, col):
        """
        Gets a list of the items along the up diagonal of row, col

        row     -- the row the diagonal must pass through
        col     -- the column the diagonal must pass through

        Returns a list of the items along the diagonal
        """

        # Move to the top right of the diagonal
        while row < self.rows:
            row += 1
        while col > 1:
            col -= 1

        # Build the list of items
        result = []
        while row >= 1 and col <= self.cols:
            result.append(self.board[row][col])
            row -= 1
            col += 1

        return result

    def _down_diagonal(self, row, col):
        """
        Gets a list of the items along the down diagonal of row, col

        row     -- the row the diagonal must pass through
        col     -- the column the diagonal must pass through

        Returns a list of the items along the diagonal
        """

        # Move to the top left of the diagonal
        while row > 1:
            row -= 1
        while col > 1:
            col -= 1

        # Build the list of items
        result = []
        while row <= self.rows and col <= self.cols:
            result.append(self.board[row][col])
            row += 1
            col += 1

        return result

    def __str__(self):
        """
        Get the game board
        """

        sep = ("-" * (2 * self.cols))[:-1]
        result = "```\n  "

        # Add numbers across top
        for i in range(0, self.cols):
            i += 1
            result += str(i) + " "
        result += "\n\n"

        # Add each row
        for r in range(self.rows):
            r += 1
            result += str(r) + " "
            for c in range(self.cols):
                entry = self.board[r][c + 1]
                result += entry + "|"
            result = result[:-1] + "\n  " + sep + "\n"

        # Remove the last sep row
        result = "\n".join(result.splitlines()[:-1])
        result += "```"
        return result
