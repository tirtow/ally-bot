from datetime import datetime
import discord


class AllyEmbed(discord.Embed):
    """
    Extends discord.Embed to add footer information to the Embed
    """

    def __init__(self, user, **kwargs):
        """
        Builds the Embed

        user    -- the discord.User to put in the footer
        kwargs  -- the args to the Embed
        """

        super().__init__(timestamp=datetime.now(), **kwargs)
        url = user.avatar_url
        if url == "":
            url = user.default_avatar_url
        self.set_footer(text=str(user), icon_url=url)


class WhenEmbed(discord.Embed):
    """
    Extends discord.Embed to use the footer to display the timestamp for a trial
    converted to the timezone for the user.
    """

    def __init__(self, msg, **kwargs):
        """
        Builds the Embed

        msg     -- the text to put in the footer
        kwargs  -- the args to the Embed
        """

        super().__init__(**kwargs)
        self.set_footer(text=msg)
