import importlib
import json
from os import listdir
from os.path import join, realpath, dirname
import time
import traceback

from ally_embed import AllyEmbed
from command.feedback_command import FeedbackCommand
from constants import COLORS
from utils import log, get_trial_class, get_commands


class SignupHandler:
    """
    Manages the Trials created for the bot.

    Handles sending any !signup commands to the proper handlers and catches any
    exceptions raised by any of the !signup commands.
    """

    def __init__(self, client, env, tirtow):
        """
        Creates a new SignupHandler building the TrialScheduler to be used to
        manage all Trial signups.

        client          -- the client running the bot
        env             -- the environment the bot is running under
        tirtow          -- the user to send errors to
        """

        self.client = client
        self.env = env
        self.trials = self._build_trials()
        self.commands = get_commands(self.client, self.trials)
        self.exception_handlers = self._get_exception_handlers()
        self.tirtow = tirtow

    async def handle(self, message):
        """
        Determines what the user is trying to do from the message received and
        directs control flow to that action.

        message -- the discord.Message that was received
        """

        split = message.content.lower().split()[1:]     # Discard the !signup
        if len(split) == 0:
            # Invalid command, print the help message
            log("Invalid signup command. Printing help message")
            await self.commands["HelpCommand"].handle(message)
        else:
            try:
                # Find the command and handle it
                op = split[0]
                for name, command in self.commands.items():
                    if command.is_alias(op):
                        if type(command) != FeedbackCommand:
                            log("%s issued command '%s'" % (str(message.author),
                                                            message.content))
                        await command.handle(message)
                        break
                else:
                    # No commands matched, default to signing user up for trial
                    log("Did not match any commands. Assume signing up for " +
                        "role")
                    await self.commands["SignupCommand"].handle(message)
            except Exception as e:
                # Handle any exceptions received
                log("Caught exception %s" % str(e))
                if type(e) in self.exception_handlers:
                    await self.exception_handlers[type(e)].handle(message)
                else:
                    log("Received unhandled error of type %s: %s" %
                        (str(type(e)), str(e)))

                    # Message tirtow
                    msg = "Received unhandled error of type `%s` at `%d`:" % \
                        (str(type(e)), time.time())
                    msg += "```%s```" % traceback.format_exc()
                    await self.client.send_message(self.tirtow, msg)

                    # Message channel
                    await self._send_unhandled_exception(message)

    def _build_trials(self):
        """
        Reads the trials in from the file building and scheduling them.

        Returns the dict mapping channel IDs to the Trial
        """

        result = {}
        for filename in listdir("trial_info"):
            # Read in the file
            with open(join("trial_info", filename)) as f:
                entry = json.loads(f.read())

            # Get the trial class
            trial_class = get_trial_class(entry["type"])

            # Initialize the trial
            result[entry["channel_id"]] = trial_class(self.client)

            # Build from file
            result[entry["channel_id"]].init_file(entry)

            # Schedule the trial
            result[entry["channel_id"]].schedule()

        return result

    def _get_exception_handlers(self):
        """
        Uses reflection to get an instance of all ExceptionHandlers.

        Returns a dict mapping the type of the exception handled to the
        ExceptionHandler.
        """

        result = {}
        path = join(dirname(realpath(__file__)), "exception_handler")
        for f in listdir(path):
            if f.endswith("_exception_handler.py"):
                name = f[:-3]               # Get rid of the .py
                split = name.split("_")     # Split into words

                # Import the module
                module = importlib.import_module("exception_handler." +
                                                 "_".join(split))

                # Create instance of class
                name = "".join(map(lambda s: s.title(), split))
                instance = getattr(module, name)(self.client, self.trials)
                result[instance.exception_type()] = instance

        return result

    async def _send_unhandled_exception(self, message):
        """
        Sends message saying something went wrong to the channel message was
        received from

        message     -- the discord.Message that was received
        """

        msg = "A robot monkey broke something. It will be dismantled and " \
              "sold for scrap and another robot monkey will fix the issue as " \
              "soon as possible."
        embed = AllyEmbed(self.client.user,
                          title="Something went wrong",
                          description=msg,
                          color=COLORS["ERROR"])
        await self.client.send_message(message.channel, embed=embed)
