from datetime import datetime
import discord
from discord.ext.commands import Bot
import os
from os.path import join
import platform
import re
import sys

from ally_embed import AllyEmbed
from constants import COLORS
from keys import keys
from signup_handler import SignupHandler
from utils import to_timestamp, log

# Command shortcuts
SHORTCUTS = {
    ":l": "!signup list",
    ":w": "!signup when",
    ":i": "!signup init",
    ":p": "!signup poll",
    ":d": "!signup drop",
    ":g": "!signup ping",
}

# The signup handler
signup_handler = None

# The channels for the welcome message
channels = {}

# Create the discord bot client
client = Bot(description="Bot by tirtow#6252", command_prefix="!")


@client.event
async def on_ready():
    """
    Performs the startup for the bot.

    Gets the channels needed to post the welcome message, sets the game playing
    message, and prints the startup message.

    Exits if fails to find any of the needed channels
    """

    global signup_handler, channels

    log("Starting up bot in %s environment" % keys["env"])

    # Get tirtow
    u = await client.get_user_info(keys["tirtow_id"])

    # Create the handler for signups
    signup_handler = SignupHandler(client, keys["env"], u)

    # Set the playing message
    log("Setting playing message")
    await client.change_presence(game=discord.Game(name='!signup help'))

    if keys["env"] == "production":
        # Get the channels
        channels = _get_channels()

        # Message tirtow the bot started
        await client.send_message(u, "ally-bot started up: " +
                                  str(to_timestamp(datetime.now())))

    _print_startup_message()


@client.event
async def on_member_join(member):
    """
    Sends the welcome message to the welcome channel when a new user joins

    Ony sends the message if the bot is being ran as the production bot to
    prevent duplicate welcome messages coming from the production bot and any
    test bots that are up.

    member -- the discord.Member that joined
    """

    if keys["env"] == "production":
        log("New user %s joined, sending message" % str(member))
        msg = "Welcome <@" + member.id + "> :heart: ! You can ask " \
              "questions about the guild in " + channels["questions"].mention +\
              ". You can request rank on discord or an invite in game in " + \
              channels["invite"].mention + ". Make sure your discord " \
              "nickname matches your in game @name first."
        await client.send_message(channels["welcome"], msg)


@client.event
async def on_message(message):
    """
    Handles checking for commands.

    Checks each message to see if it matches one of the commands that the bot
    responds to handling the command if it finds one.

    message -- the discord.Message that was received
    """

    command = message.content.lower()
    if command.startswith("!singup") or command.startswith("::singup"):
        # Singup fun
        await _send_singup_message(message)
    elif command.startswith("!signup") or command.startswith("::signup"):
        # User is accessing the signup commands
        await signup_handler.handle(message)
    elif command.startswith("::"):
        # Shortcut for accessing signup commands
        message.content = message.content.replace("::", "!signup ")
        await signup_handler.handle(message)
    elif _handle_shortcut(message):
        # Command shortcut
        await signup_handler.handle(message)
    elif command == "!ping":
        # Ping command to check if bot is alive and responding
        await client.send_message(message.channel, "Pong!")
    elif _is_good_morning(message):
        await _add_morning_reactions(message)


@client.event
async def on_channel_delete(channel):
    """
    Handles when a channel is deleted to cleanup that Trial.

    If there is a Trial in the deleted channel, delete that Trial.

    channel -- the discord.Channel that was deleted
    """

    try:
        signup_handler.trials[channel.id].unschedule()
        os.remove(join("trial_info", channel.id + ".json"))
    except KeyError:
        # There was no Trial scheduled
        pass


def _handle_shortcut(message):
    """
    Replaces the shortcut in message if one exists.

    Modifies message.content replacing the shortcut with the full command if a
    shortcut is found.

    message -- the discord.Message to check.

    Returns True if finds a shortcut, False otherwise
    """

    split = message.content.lower().split()
    if len(split) > 0 and split[0] in SHORTCUTS:
        split[0] = SHORTCUTS[split[0]]
        message.content = " ".join(split)
        return True
    return False


def _is_good_morning(message):
    """
    Checks if the message sent was a good morning message

    message     -- the discord.Message to check

    Returns True if was good morning, False otherwise
    """

    pattern = r".*([Gg]+[Oo]{2,}[Dd]+)?\s*" \
              r"([Mm]+[Oo]+[Rr]+[Nn]+[Ii]+[Nn]+[Gg]+).*"
    return re.match(pattern, message.content) is not None


async def _add_morning_reactions(message):
    """
    Adds a morning reaction to the given message

    message     -- the discord.Message to react to
    """

    await client.add_reaction(message, "\U00002600")


async def _send_singup_message(message):
    """
    Sends a funsies "singup" message

    message -- the message that was received
    """

    notes = "\U0000266C" * 15
    msg = "Eat 'em up, eat 'em up, eat 'em up, eat 'em up\n" \
          "Eat 'em up, eat 'em up, eat 'em up, eat 'em up\n" \
          "Reese's Puffs, Reese's Puffs\n" \
          "Eat 'em up, eat 'em up, eat 'em up, eat 'em up\n" \
          "Reese's Puffs, Reese's Puffs\n" \
          "Eat 'em up, eat 'em up, eat 'em up, eat 'em up\n\n" \
          "(Wow) I got Reese's puffs in my bowl\n" \
          "(Wow) Nowadays on cruise control\n" \
          "(Wow) I got Reese's puffs in my bowl\n" \
          "(Wow) And just like that I'm on a roll\n"
    msg = notes + "\n" + msg + notes

    embed = AllyEmbed(user=client.user,
                      title="Singup",
                      description=msg,
                      color=COLORS["SUCCESS"])
    await client.send_message(message.channel, embed=embed)


def _get_channels():
    """
    Gets all of the channels specified by the key file.

    Exits if fails to find a channel.

    Returns a dict mapping the name of the channel to the discord.Channel
    """

    result = {}
    for name, channel_id in keys["channels"].items():
        result[name] = client.get_channel(channel_id)
        if result[name] is None:
            print("Failed to find the channel " + name + ". Exitting")
            sys.exit(1)

    return result


def _print_startup_message():
    """
    Prints the startup message.
    """

    print("Current Discord.py Version: {} | Current Python Version: {}"
          .format(discord.__version__, platform.python_version()))
    print("--------")
    print("Use this link to invite {}:".format(client.user.name))
    print("https://discordapp.com/oauth2/authorize?client_id={}&scope=bot"
          "&permissions=8".format(client.user.id))
    print("--------")
    print("--------")
    print("Created by tirtow#6252\n")


# Run the bot
if __name__ == "__main__":
    client.run(keys["bot_token"])
