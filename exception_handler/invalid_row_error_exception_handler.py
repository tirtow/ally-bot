from ally_exceptions import InvalidRowError
from exception_handler.exception_handler import ExceptionHandler


class InvalidRowErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle InvalidRowError
    """

    async def handle(self, message, title="Invalid Row"):
        """
        Sends message telling the user they gave an invalid row

        message     -- the discord.Message that was received
        title       -- the title for the Embed. (default "Invalid Row")
        """

        await self.send_error_message(message.author, title,
                                      "Invalid row given")

    def exception_type(self):
        """
        Returns the InvalidRowError type
        """

        return InvalidRowError
