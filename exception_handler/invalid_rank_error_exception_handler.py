from ally_exceptions import InvalidRankError
from exception_handler.exception_handler import ExceptionHandler


class InvalidRankErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle InvalidRankErrors.
    """

    async def handle(self, message, title="Signup Error"):
        msg = "You must be an Embroidered, Totally Turpen, or Imposing " + \
              "Iridium to signup for this trial. See <#425447912172945428> " + \
              "for more information."
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Returns the InvalidRankError type.
        """

        return InvalidRankError
