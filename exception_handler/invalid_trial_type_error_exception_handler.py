from ally_exceptions import InvalidTrialTypeError
from exception_handler.exception_handler import ExceptionHandler


class InvalidTrialTypeErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle InvalidTrialTypeErrors.
    """

    async def handle(self, message, title="Signup Error"):
        """
        Sends message telling the user they gave an invalid trial type for
        creating a trial.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Error"
        """

        msg = "Failed to create trial. Invalid trial type given"
        if message.content.lower().endswith("sassy"):
            msg = "Yo how hard is it to type the trial type correctly."
            title = "Sassy Error"
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Return the InvalidTrialTypeError type.
        """

        return InvalidTrialTypeError
