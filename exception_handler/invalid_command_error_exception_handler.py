from ally_exceptions import InvalidCommandError
from exception_handler.exception_handler import ExceptionHandler


class InvalidCommandErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle InvalidCommandErrors.
    """

    async def handle(self, message, title="Signup Error"):
        """
        Informs the user that the command they entered was invalid.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Error"
        """

        msg = "Invalid command. Use !signup help for help."
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Return the InvalidCommandError type
        """

        return InvalidCommandError
