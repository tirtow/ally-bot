from exception_handler.exception_handler import ExceptionHandler


class KeyErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle KeyErrors.
    """

    async def handle(self, message, title="Signup Error"):
        """
        Sends message that no trial was found in the channel the message was
        sent in.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Error"
        """

        msg = "There is no trial for " + message.channel.mention
        if message.content.lower().endswith("sassy"):
            msg = "Do I look like a magician? Why you be thinking I can " + \
                  "make a trial appear out of nowhere???"
            title = "Sassy Error"
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Return the KeyError type
        """

        return KeyError
