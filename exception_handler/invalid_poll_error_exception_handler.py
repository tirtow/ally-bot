from ally_exceptions import InvalidPollError
from exception_handler.exception_handler import ExceptionHandler


class InvalidPollErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle InvalidPollErrors.
    """

    async def handle(self, message, title="Signup Poll Error"):
        """
        Sends message that the user attempted to create a poll but the command
        was invalid.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Poll Error"
        """

        msg = "Usage: !signup poll <description> --options <options>"
        if message.content.lower().endswith("sassy"):
            msg = "Poll options:\nA. I don't know what I'm doing.\nB. I " + \
                  "don't know what I'm doing."
            title = "Sassy Error"
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Return the InvalidPollError type.
        """

        return InvalidPollError
