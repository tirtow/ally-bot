from ally_exceptions import SpotAlreadyTakenError
from exception_handler.exception_handler import ExceptionHandler


class SpotAlreadyTakenErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle SpotAlreadyTakenError
    """

    async def handle(self, message, title="Spot Already Taken"):
        """
        Sends message telling the user they gave an invalid row

        message     -- the discord.Message that was received
        title       -- the title for the Embed. (default "Spot Already Taken")
        """

        await self.send_error_message(message.author, title,
                                      "That spot is already taken.")

    def exception_type(self):
        """
        Returns the SpotAlreadyTakenError type
        """

        return SpotAlreadyTakenError
