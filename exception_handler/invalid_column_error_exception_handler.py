from ally_exceptions import InvalidColumnError
from exception_handler.exception_handler import ExceptionHandler


class InvalidColumnErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle InvalidColumnError
    """

    async def handle(self, message, title="Invalid Column"):
        """
        Sends message telling the user they gave an invalid column

        message     -- the discord.Message that was received
        title       -- the title for the Embed. (default "Invalid Column")
        """

        await self.send_error_message(message.author, title,
                                      "Invalid column given")

    def exception_type(self):
        """
        Returns the InvalidColumnError type
        """

        return InvalidColumnError
