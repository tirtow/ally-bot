from ally_exceptions import SignupsClosedError
from exception_handler.exception_handler import ExceptionHandler


class SignupsClosedErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle the SignupsClosedError.
    """

    async def handle(self, message, title="Signup Error"):
        """
        Sends message that signups have not been opened yet for the trial in
        the channel the message was sent in.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Error"
        """

        msg = "Signups are not open yet for " + message.channel.mention
        if message.content.lower().endswith("sassy"):
            msg = "Go to the store and buy some patience broski"
            title = "Sassy Error"
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Return the SignupsClosedError type
        """

        return SignupsClosedError
