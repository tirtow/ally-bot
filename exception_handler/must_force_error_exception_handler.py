from ally_exceptions import MustForceError
from exception_handler.exception_handler import ExceptionHandler


class MustForceErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle MustForceErrors.
    """

    async def handle(self, message, title="Signup Error"):
        """
        Sends message telling the user they must force the command.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Error"
        """

        msg = "Signups are currently open. Doing this command will delete " \
              "any signups.\n\nYou can force the command with --force"
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Returns the MustForceError type.
        """

        return MustForceError
