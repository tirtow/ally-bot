from ally_exceptions import TrialAlreadyScheduledError
from exception_handler.exception_handler import ExceptionHandler


class TrialAlreadyScheduledErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle TrialAlreadyScheduledErrors.
    """

    async def handle(self, message, title="Signup Error"):
        """
        Sends message telling the user a trial has already been created in
        that channel.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Error"
        """

        msg = "Failed to create trial. A trial has already been scheduled " + \
              "in " + message.channel.mention
        if message.content.lower().endswith("sassy"):
            msg = "Yo dawg I'm already been overworked here what you be " + \
                  "double booking me for??????"
            title = "Sassy Error"
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Return the TrialAlreadyScheduledError type.
        """

        return TrialAlreadyScheduledError
