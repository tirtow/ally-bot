from ally_exceptions import InsufficentPermissionsError
from exception_handler.exception_handler import ExceptionHandler


class InsufficentPermissionsErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle InsufficentPermissionsErrors.
    """

    async def handle(self, message, title="Signup Error"):
        """
        Sends message telling the user they gave an invalid trial type for
        creating a trial.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Error"
        """

        msg = "You must be an officer to do that"
        if message.content.lower().endswith("sassy"):
            msg = "Oh I know you just did not try to do something you " + \
                  "shouldn't be doing."
            title = "Sassy Error"
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Returns the InsufficentPermissionsError type.
        """

        return InsufficentPermissionsError
