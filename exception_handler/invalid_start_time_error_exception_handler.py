from ally_exceptions import InvalidStartTimeError
from exception_handler.exception_handler import ExceptionHandler


class InvalidStartTimeErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle InvalidStartTimeErrors.
    """

    async def handle(self, message, title="Signup Error"):
        """
        Sends message telling the user they gave an invalid start time for
        creating a trial.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Error"
        """

        # Remove the trial from the dict
        if message.channel.id in self.trials:
            del self.trials[message.channel.id]

        # Send message
        msg = "Failed to create trial. The start time is invalid."
        if message.content.lower().endswith("sassy"):
            msg = "Bruh you think we got a time machine up in here????"
            title = "Sassy Error"
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Return the InvalidStartTimeError type.
        """

        return InvalidStartTimeError
