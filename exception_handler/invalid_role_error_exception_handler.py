from ally_exceptions import InvalidRoleError
from exception_handler.exception_handler import ExceptionHandler


class InvalidRoleErrorExceptionHandler(ExceptionHandler):
    """
    Inherits from ExceptionHandler to handle InvalidRoleErrors.
    """

    async def handle(self, message, title="Signup Error"):
        """
        Sends message telling the user they gave an invalid role.

        message -- the discord.Message that was received
        title   -- the title for the Embed. Default is "Signup Error"
        """

        msg = "Invalid role. Available roles: %s" % \
            str(self.trials[message.channel.id].roles())
        await self.send_error_message(message.channel, title, msg)

    def exception_type(self):
        """
        Returns the InvalidRoleError type.
        """

        return InvalidRoleError
