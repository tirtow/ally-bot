from role.role import Role


class TankRole(Role):
    """
    Inherits from Role to implement the Tank role.
    """

    def __init__(self, members=(), server=None):
        """
        Initialize the name of this Role
        """

        super().__init__(members, server)
        self.name = "Tank"

    def abbr(self):
        """
        Return the abbreviation for this Role.
        """

        return "T"

    def aliases(self):
        """
        Returns a tuple of the aliases for this role.
        """

        return ("tank", "tenk", "tankytank", "igethitintheface", "t")
