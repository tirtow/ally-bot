"""
The roles used by trials for ally-bot.

Roles are loaded in at runtime using reflection. Only commands in files
ending in "_role.py" are loaded in.

Roles must follow the naming structure:
- filename:   some_name_role.py
- class name: SomeNameRole.py

Roles **must** respond to the methods that `Role` responds to in order to
integrate properly with ally-bot. See `help(Role)` for more information.
"""
