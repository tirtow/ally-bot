import re


class Role:
    """
    Provides the base implementation for a Role users can signup for.

    Roles provide the options for users to signup for and manage the list of
    users who have signed up for the Role.

    Inheriting classes MUST provide the following attributes:
    - self.name: the name of the role

    Inheriting classes MUST implement the following methods:
    - abbr(): gets the abbreviation for this Role
    """

    def __init__(self, members=(), server=None):
        """
        Initialize the dict of users for this Role.
        """

        self.backup_role = False

        # dict to map the order of members to the member
        self.signups = {}

    def load(self, members, server):
        """
        Loads in the members for this Role.

        members     -- the dict of user information.
        server      -- the discord.Server the bot is running on.
        """

        for order, member_id in members.items():
            self.signups[int(order)] = server.get_member(member_id)

    def clear(self):
        """
        Clears the list of signed up users.
        """

        self.signups = {}

    def signup(self, member, order=None):
        """
        Signs the given member up for this Role.

        member  -- the discord.Member to signup.
        order   -- the order in the signups list for the member. If None
                   defaults to the end of the list. (default None)

        Returns True if signed member up, False otherwise
        """

        if member in self:
            # Member is already signed up for this Role
            return False

        if order is None:
            # Default to putting at end of the list
            order = 0
            if len(self.signups) > 0:
                order = sorted(self.signups.keys()).pop() + 1

        # Sign the member up
        self.signups[order] = member
        return True

    def unsignup(self, to_remove):
        """
        Removes the given member from this Role.

        to_remove   -- the discord.Member to remove.

        Returns True if successfully removed the member, False otherwise
        """

        # Find the key to remove
        for order, member in self.signups.items():
            if to_remove == member:
                # Drop entry
                del self.signups[order]
                return True
        return False

    def list(self, user_roles):
        """
        Get the string of the users using their nickname.

        user_roles  -- the dict mapping each member to the roles they have
                       signed up for.

        Returns the string of users.
        """

        return self._user_str(user_roles, self._user_nick)

    def ping(self, user_roles):
        """
        Get the string of the users tagging each user.

        user_roles  -- the dict mapping each member to the roles they have
                       signed up for.

        Returns the string of users.
        """

        return self._user_str(user_roles, self._user_tag)

    def abbr(self):
        """
        Returns the string of the abbreviation for this Role.

        Must be implemented by inheriting classes.
        """

        raise NotImplementedError("abbr() must be implemented by " +
                                  str(type(self)))

    def aliases(self):
        """
        Returns an iterable of the aliases for this Role.

        Must be implemented by inheriting classes.
        """

        raise NotImplementedError("aliases() must be implemented by " +
                                  str(type(self)))

    def class_name(self):
        """
        Returns the name of the class of this Role
        """

        pattern = "\.[a-z_]+\."                     # Find 'name'
        name = re.search(pattern, str(type(self)))  # Get the name
        return name.group(0)[1:-1]                  # Remove the 's

    def is_backup(self):
        """
        Gets whether or not this role is a backup role
        """

        return self.backup_role

    def to_dict(self):
        """
        Returns a the internal dict with members replaced by their ids.
        """

        result = {}
        for order, member in self.signups.items():
            result[order] = member.id
        return result

    def __contains__(self, val):
        """
        Checks if the given member has already signed up for this Role.

        val     -- the member to check for

        Returns True if the member has already signed up, False otherwise
        """

        for order, member in self.signups.items():
            if member == val:
                return True
        return False

    def __len__(self):
        """
        Get the number of signups.
        """

        return len(self.signups)

    def __str__(self):
        """
        Return the name of this Role
        """

        return self.name

    def _user_str(self, user_roles, uf):
        """
        Builds a string of the users signed up for this Role.

        user_roles  -- the dict mapping each member to the roles they have
                       signed up for.
        uf          -- the unary function to use to get the string to use for
                       each member.

        Returns the string
        """

        result = "__%s__\n" % str(self)

        for i, order in enumerate(sorted(self.signups.keys())):
            member = self.signups[order]

            # Append the name
            result += "- %d. %s" % (i + 1, uf(member))

            # Append any other roles the user signed up for
            if len(user_roles[member]) > 1:
                result += " %s" % self._roles_to_str(user_roles[member])

            # End line
            result += "\n"

        return result

    def _user_nick(self, member):
        """
        Returns the nickname of the member if they have one set, display name
        otherwise.
        """

        if member.nick is not None:
            return member.nick
        return member.display_name

    def _user_tag(self, member):
        """
        Returns the string to use to tag the member.
        """

        return "<@%s>" % member.id

    def _roles_to_str(self, user_roles):
        """
        Converts user_roles into a string.
        """

        # Return empty string if user_roles is empty
        if len(user_roles) == 0:
            return ""

        # Add first item
        it = iter(user_roles)
        result = "(%s" % next(it)

        # Add rest of the items
        for role in it:
            result += ", %s" % role
        result += ")"

        return result
