from role.role import Role


class MeleeRole(Role):
    """
    Inherits from Role to implement the Melee DPS role.
    """

    def __init__(self, members=(), server=None):
        """
        Initialize the name of this Role
        """

        super().__init__(members, server)
        self.name = "Melee DPS"

    def abbr(self):
        """
        Return the abbreviation for this Role.
        """

        return "M"

    def aliases(self):
        """
        Returns a tuple of the aliases for this role.
        """

        return ("melee", "meleedps", "m", "mdps")
