from role.role import Role


class HealerRole(Role):
    """
    Inherits from Role to implement the Healer role.
    """

    def __init__(self, members=(), server=None):
        """
        Initialize the name of this Role
        """

        super().__init__(members, server)
        self.name = "Healer"

    def abbr(self):
        """
        Return the abbreviation for this Role.
        """

        return "H"

    def aliases(self):
        """
        Returns a tuple of the aliases for this role.
        """

        return ("healer", "heal", "heals", "healyheal", "isaveyourlife", "h")
