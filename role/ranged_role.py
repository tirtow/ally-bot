from role.role import Role


class RangedRole(Role):
    """
    Inherits from Role to implement the Ranged DPS role.
    """

    def __init__(self, members=(), server=None):
        """
        Initialize the name of this Role
        """

        super().__init__(members, server)
        self.name = "Ranged DPS"

    def abbr(self):
        """
        Return the abbreviation for this Role.
        """

        return "R"

    def aliases(self):
        """
        Returns a tuple of the aliases for this role.
        """

        return ("ranged", "range", "rangeddps", "rangedps", "r", "rdps")
