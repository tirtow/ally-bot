from role.role import Role


class DpsRole(Role):
    """
    Inherits from Role to implement the DPS role.
    """

    def __init__(self, members=(), server=None):
        """
        Initialize the name of this Role
        """

        super().__init__(members, server)
        self.name = "DPS"

    def abbr(self):
        """
        Return the abbreviation for this Role.
        """

        return "D"

    def aliases(self):
        """
        Returns a tuple of the aliases for this role.
        """

        return ("dps", "wackwack", "pewpew", "istandinred")
