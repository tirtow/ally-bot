from role.tank_role import TankRole


class BackupTankRole(TankRole):
    """
    Inherits from Role to implement the Backup Tank role.
    """

    def __init__(self, members=(), server=None):
        """
        Initialize the name of this Role
        """

        super().__init__(members, server)
        self.name = "Backup Tank"
        self.backup_role = True

    def abbr(self):
        """
        Return the abbreviation for this Role.
        """

        return "B%s" % super().abbr()

    def aliases(self):
        """
        Returns a tuple of the aliases for this role.
        """

        return list(map(lambda s: "backup %s" % s, super().aliases())) + \
            list(map(lambda s: "%s backup" % s, super().aliases()))

    def list(self, user_roles):
        """
        Return an empty string if there are no signups, Role::list() otherwise.
        """

        if len(self.signups) == 0:
            return ""
        return super().list(user_roles)

    def ping(self, user_roles):
        """
        Return an empty string if there are no signups, Role::ping() otherwise.
        """

        if len(self.signups) == 0:
            return ""
        return super().ping(user_roles)
