# Signups commands
COMMANDS = {
    "SCHEDULE": ("schedule", "init", "initialize", "start", "create"),
    "UNSCHEDULE": ("unschedule",),
    "DROP": ("drop", "unsignup"),
    "LIST": ("list", "who"),
    "PING": ("ping", "tag"),
    "WHEN": ("when", "impatient", "omgwhenwillthisthingstart"),
    "POLL": ("vote", "poll"),
    "HELP": ("help",),
    "MODIFY": ("modify", "change", "update"),
    "STATUS": ("status", "pulse"),
    "FIXTIMESTAMP": ("fixtimestamp", "fix_timestamp", "autocorrect", "autoc"),
    "TIME": ("time", ),
}

# The available roles
ROLES = {
    "TANK": ("tank", "tankytank", "tenk", "igethitintheface"),
    "HEALER": ("healer", "heal", "heals", "healyheal", "isaveyourlife"),
    "DPS": ("dps", "wackwack", "pewpew", "istandinred"),
    "RANGED": ("range", "ranged", "mag", "magic", "magicka", "r", "rdps"),
    "MELEE": ("melee", "stam", "stamina", "m", "mdps"),
}

# The days of the week
DAYS = {
    0: ("monday", "mon", "m", "0"),
    1: ("tuesday", "tue", "tues", "t", "1"),
    2: ("wednesday", "wed", "weds", "w", "2"),
    3: ("thursday", "thu", "thur", "th", "r", "3"),
    4: ("friday", "fri", "f", "4"),
    5: ("saturday", "sat", "s", "5"),
    6: ("sunday", "sun", "n", "6"),
}
